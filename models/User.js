
function User(name, fullname, email) {
	this.name = name;
	this.fullname = fullname;
    this.email = email;
};

User.prototype = {

    name: null,

    fullname: null,

    email: null

};

User.all = function() {
	return users;
}

var users = [
    new User("Hua", "华叔", "zhangweihua@noxus.cn"),
    new User("Jun", "俊", "lijun@noxus.cn"),
    new User("Liangzi", "梁子", "liangzi@noxus.cn"),
    new User("Rui", "蕊爷", "wangrui@noxus.cn"),
    new User("Mawenhui", "马老板", "mawenhui@noxus.cn"),
    new User("Wuyue", "乐姐", "wuyue@noxus.cn"),
    new User("Cuihaichen", "海晨", "cuihaichen@noxus.cn"),
    new User("Liweiwei", "小伟", "liweiwei@noxus.cn"),
    new User("Shijiangwang", "石爷", "shijiangwang@noxus.cn"),
    new User("Wudeyong", "德勇", "wudeyong@noxus.cn"),
    new User("Luchao", "陆超", "luchao@noxus.cn"),
    new User("Sunjiaxiang", "家祥", "sunjiaxiang@noxus.cn"),
    new User("Songjiayi", "老宋", "songjiayi@noxus.cn"),
    new User("Kechangyi", "昌老师", "kechangyi@noxus.cn"),
    new User("Yangbo", "杨波", "yangbo@noxus.cn"),
    new User("Suniaowen", "叔鸟", "sushaowen@noxus.cn"),
    new User("Guolin", "郭老板", "guolin@noxus.cn")
];

module.exports = User;