var Swallower = Swallower || {};

Swallower.views = Swallower.views || {};

/**
 * Login view.
 *
 * @namespace Swallower.views
 * @class Login
 * @constructor
 * @param users {array} Array contains user object
 */
Swallower.views.Login = function(users) {
    var self = this;
    this.users = users;
    this.usersContainer = $("#users");
    this.bricks = {};
    $(window).bind("Authenticated", function(event, user) {
        self.onAuthenticated(user);
    });
    $(window).bind("Logout", function(event, user) {
        self.onLogout();
    });
};

Swallower.views.Login.prototype = {

    users: null,

    usersContainer: null,

    bricks: null,

    current: null,

    render: function() {
        var self = this;
        $.each(this.users, function(i, user) {
            var brick = new Swallower.views.Login.Brick(user);
            self.bricks[user.name] = brick;
            self.usersContainer.append(brick.el);
        });
    },

    onAuthenticated: function(user) {
        var self = this, currentBrick = this.bricks[user.name];
        this.current = user;
        currentBrick.pin();
        $.each(this.bricks, function(name, brick) {
            if (brick != currentBrick) {
                brick.hide();
            }
        });
        setTimeout(function() {
            currentBrick.expand(function() {
                self.usersContainer.height(currentBrick.el.outerHeight(true));
                self.onCompleted();
            });
        }, 1100);
    },

    onLogout: function(user) {
        var self = this, currentBrick = this.bricks[this.current.name];
        self.usersContainer.height("auto");
        currentBrick.collapse(function() {
            $.each(self.bricks, function(name, brick) {
                if (brick != currentBrick) {
                    brick.show();
                }
            });
            setTimeout(function() {
                currentBrick.unpin();
                currentBrick.rollToSummary();
            }, 1100);
        });
    },
    
    onCompleted: function() {
        var currentBrick = this.bricks[this.current.name],
            color = currentBrick.backgroundColorAnim.target;
        $(window).trigger("LoginCompleted", [this.current, color]);
    }

};

/**
 * Brick element holder.
 *
 * @namespace Swallower.views.Login
 * @class Brick
 * @constructor
 * @param user {object} User object
 */
Swallower.views.Login.Brick = function(user) {
    var self = this;
    this.user = user;
    this.backgroundColorAnim = { 
        origin: "#f2f6f8", 
        target: Swallower.utils.hsl2rgb(Math.random(), 0.2, 0.6) 
    };
    this.el = $("<li>&nbsp;</li>")
        .css("color", this.colorAnim.origin)
        .css("background-color", this.backgroundColorAnim.origin)
        .hover(function() {
            self.onMouseOver();
        }, function() {
            self.onMouseOut();
        })
        .click(function() {
            self.onClick();
        });
    this.elSummary = $("<div></div>").text(user.name).addClass("name").appendTo(this.el);
    this.elDetails = $("<div></div>").addClass("details").appendTo(this.el);
    $("<em></em>").text(user.fullname).appendTo(this.elDetails);
    $("<span></span>").text(user.email).appendTo(this.elDetails);
};

Swallower.views.Login.Brick.prototype = {

    width: 260,

    margin: 32,

    backgroundColorAnim: null,  // Should set a random color

    colorAnim: { origin: "#ddd", target: "white" },

    user: null,

    el: null,

    elSummary: null,

    elDetails: null,

    isPinned: false,

    position: null,

    isExpanded: false,

    isHide: false,

    pin: function() {
        this.isPinned = true;
    },

    unpin: function() {
        this.isPinned = false;
    },

    show: function() {
        this.el.css("cursor", "pointer").fadeTo(1000, 1);
        this.isHide = false;
    },

    hide: function() {
        this.isHide = true;
        this.el.css("cursor", "default").fadeTo(1000, 0);
    },

    expand: function(complete) {
        var self = this, el = this.el,
            offset = this.el.offset();
        this.position = el.position();
        el.css("position", "absolute")
            .css("z-index", 1000)
            .offset(offset);
        el.animate({ "top": 0 }, this.position.top * 2.5, function() {
            el.animate({ "width": "100%", "left": 0 }, 800, function() {
                if (complete) complete.call(self);
            });
        });
        this.isExpanded = true;
    },

    collapse: function(complete) {
        var self = this, el = this.el;
        this.isExpanded = false;
        el.animate({ "width": this.width + "px", "left": self.position.left }, 800, function() {
            el.animate({ "top": self.position.top }, self.position.top * 2.5, function() {
                el.css("top", "").css("left", "").css("position", "").css("z-index", "");
                self.position = null;
                if (complete) complete.call(self);
            });
        });
    },

    rollToDetails: function() {
        var innerWidth = this.width - this.margin * 2,
            time = 400;
        this.el.animate({
            "background-color": this.backgroundColorAnim.target.toString(), 
            "color": this.colorAnim.target
        }, time);
        this.elSummary.animate({
            "left": (innerWidth - this.elSummary.width()) + "px",
            "opacity": 0
        }, time);
        this.elDetails.animate({
            "right": (innerWidth - this.elDetails.width()) + "px",
            "opacity": 1
        }, time);
    },

    rollToSummary: function() {
        var time = 200;
        this.el.animate({
            "background-color": this.backgroundColorAnim.origin, 
            "color": this.colorAnim.origin
        }, time);
        this.elSummary.animate({
            "left": 0,
            "opacity": 1
        }, time);
        this.elDetails.animate({
            "right": 0,
            "opacity": 0
        }, time);
    },

    onMouseOver: function() {
        if (this.isHide || this.isPinned) {
            return;
        }
        this.rollToDetails();
    },

    onMouseOut: function() {
        if (this.isHide || this.isPinned) {
            return;
        }
        this.rollToSummary();
    },

    onClick: function() {
        if (this.isHide) {
            return;
        }
        if (!this.isExpanded) {
            $(window).trigger("Authenticated", this.user);
        } else {
            $(window).trigger("Logout", this.user);
        }
    }

};

/**
 * Menu view.
 * 
 * @namespace Swallower.views
 * @class Menu
 * @constructor
 * @param restaurant {object} Restaurant data
 */
Swallower.views.Menu = function(restaurant) {
    var self = this;
    this.container = $("#menu");
    this.restaurant = restaurant;
    $(window).bind("LoginCompleted", function(event, user, color) {
        self.user = user;
        self.color = color;
        self.show();
    });
};

Swallower.views.Menu.prototype = {

    container: null,

    restaurant: null,

    user: null,

    color: null,

    show: function() {
        var self = this;
        this.container.show();
        $.each(this.restaurant.categories, function(i, category) {
            self._showCategory(category);
        });
    },

    _showCategory: function(category) {
        var self = this, elCategory, elDishes, color = self._getRelatedColor();
        elCategory = $("<li></li>").addClass("category").appendTo(this.container);
        $("<h3></h3>").css("border-bottom", "2px solid " + color).css("color", color.toString()).text(category.name).appendTo(elCategory);

        // Show dishes
        elDishes = $("<ul></ul>").addClass("dishes").appendTo(elCategory);
        $.each(category.dishes, function(i, dish) {
            self._showDish(elDishes, dish);
        });
    },

    _showDish: function(elDishes, dish) {
        var self = this, elDish, color = this._getRelatedColor();
        elDish = $("<li></li>").addClass("dish")
            .hover(function() {
                elDish.css("background-color", color.toString()).css("color", "white");
            }, function() {
                elDish.css("background-color", "").css("color", "");
            })
            .appendTo(elDishes);
        $("<em></em>").text(dish.name).appendTo(elDish);
        $("<span></span>").text(dish.price + "元").appendTo(elDish);
    },

    _getRelatedColor: function() {
        var hsl = this.color.toHSL();
        hsl.h = hsl.h + 0.5 - 1;
        hsl.s = 0.1;
        // hsl.l = 0.4;
        return hsl.toRGB();
    }

};
