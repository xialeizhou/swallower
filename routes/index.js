var User = require("../models/User"),
    Storage = require("../models/Storage");

/*
 * GET home page.
 */

exports.index = function(req, res) {
    var users = User.all();
    res.render('index', { users: users, restaurants: Storage.restaurants });
};
